FROM alpine:3.8
LABEL maintainer="Igor Nehoroshev <harry@hashdivision.com>"

RUN apk add --no-cache openvpn easy-rsa \
    && mkdir -p /etc/openvpn/data

COPY server.conf /etc/openvpn/server.conf
COPY client.conf /client.conf
COPY scripts/setup_server /setup_server
COPY scripts/create_client /create_client
WORKDIR /etc/openvpn

CMD mkdir -p /dev/net && \
    if [ ! -c /dev/net/tun ] ; then mknod /dev/net/tun c 10 200 ; fi \
    && openvpn --config server.conf
